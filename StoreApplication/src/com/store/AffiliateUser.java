package com.store;

/**
 * Created by uwakchau on 12/20/2018.
 */
public class AffiliateUser implements Users {

	String affiliateUserName;
    String address;
    String email;
    Boolean grocery;
    double bill;
    
    public double getBill() {
		return bill;
	}

	public void setBill(double bill) {
		this.bill = bill;
	}
    
	public Boolean getGrocery() {
		return grocery;
	}

	public void setGrocery(Boolean grocery) {
		this.grocery = grocery;
	}

	public String getAffiliateUserName() {
		return affiliateUserName;
	}

	public void setAffiliateUserName(String affiliateUserName) {
		this.affiliateUserName = affiliateUserName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getUserType() {
		
		return Constants.AFFILIATEUSER;
	}

	@Override
	public double getUserDiscount() {
		return 0.10;
	}

	@Override
	public Boolean IsGroceryItem(Boolean val) {
			setGrocery(val);
		return getGrocery();
	}
	
	@Override
	public double billAmount(double billAmt) {
		setBill(billAmt);
		return getBill();
	}
	
}
