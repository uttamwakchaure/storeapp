package com.store;

public class CustomerUser implements Users{
	String customerName;
    String address;
    String email;
    Boolean grocery;
    double bill;
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getGrocery() {
		return grocery;
	}
	public void setGrocery(Boolean grocery) {
		this.grocery = grocery;
	}
	public double getBill() {
		return bill;
	}
	public void setBill(double bill) {
		this.bill = bill;
	}
	@Override
    public String getUserType() {
    	  return Constants.CUSTOMERUSER;
    }	@Override
	public double getUserDiscount() {
		return 0.05;
	}

	@Override
	public Boolean IsGroceryItem(Boolean val) {
			setGrocery(val);
		return getGrocery();
	}
	
	@Override
	public double billAmount(double billAmt) {
		setBill(billAmt);
		return getBill();
	}
    

}
