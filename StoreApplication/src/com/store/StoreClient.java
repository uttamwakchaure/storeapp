package com.store;

import java.util.Scanner;

public class StoreClient {
	
	public static void main(String []args){
		
		
		 Scanner in = new Scanner(System.in);
		 System.out.println("\n*****");
		 System.out.println("\nEnter\n 1 for EMPLOYEEUSER \n 2 for AFFILIATEUSER \n 3 for CUSTOMERUSER \n Then Enter Bill AMOUNT and  true for Grocery \n false for Non-Grocery Items\n");
		 System.out.println("Enter an integer for User Type");
		  
		 UserFactory fact = new UserFactory();
		 Users empUserObj = null,affUserObj=null,customerUserObj=null,emp;
		 Boolean userGrocery = null;
		 
	      String userType = in.nextLine();
	      
	      System.out.println("Enter an Bill Amount");	     
	      double billAmount = in.nextDouble();
	      System.out.println("Enter a TRUE OR FALSE for Grocery");
	      boolean grocery = in.nextBoolean();
	      EmployeeUser empUser = null;
	      AffiliateUser affUser = null;
	      CustomerUser custUser = null;
	      
	      if(userType.equals("1")){
	 		 	empUser = new EmployeeUser();
		    	empUserObj = fact.getInstance(Constants.EMPLOYEEUSER);
		    	empUser.setBill(billAmount);
				userGrocery = empUserObj.IsGroceryItem(grocery);
				UtilityClass.calculateDiscount(empUser,billAmount,userGrocery);
		    }else if(userType.equals("2")){
		    	affUser = new AffiliateUser();
		    	affUserObj = fact.getInstance(Constants.AFFILIATEUSER);
		    	affUser.setBill(billAmount);
		    	userGrocery = affUserObj.IsGroceryItem(grocery);
		    	UtilityClass.calculateDiscount(affUser,billAmount,userGrocery);
		    }else if(userType.equals("3")){
		    	custUser = new CustomerUser();
		    	customerUserObj = fact.getInstance(Constants.CUSTOMERUSER);
		    	custUser.setBill(billAmount);
		    	userGrocery = customerUserObj.IsGroceryItem(grocery);
		    	UtilityClass.calculateDiscount(custUser,billAmount,userGrocery);
		    }
		
	}	
	
		
}
