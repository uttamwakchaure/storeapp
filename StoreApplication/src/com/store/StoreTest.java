package com.store;

import static org.junit.Assert.*;

import org.junit.Test;

public class StoreTest {

	@Test
	public void testForObjectNull() {        
		Users empUser =  new EmployeeUser();
        System.out.println("\nObject should not be null");
        assertNotNull(empUser);
       
	}
	@Test
	public void testforDiscountWithGrocery() {
		Users empUser =  new EmployeeUser();
        double billAmt=990.0;
        boolean grocery = true;
        
        System.out.println("\nTest for applicable discount with grocery");
        double output = UtilityClass.calculateDiscount(empUser, billAmt, grocery);
        assertEquals("990.0",Double.toString(output)); 
	}
	
	//-ve Test
	@Test
	public void testforDiscountWithOutGrocery() {		
        Users empUser =  new EmployeeUser();
        double billAmt=990.0;
        boolean grocery = false;
        
        System.out.println("\nTest for applicable discount without grocery");
        double output1 = UtilityClass.calculateDiscount(empUser, billAmt, grocery);
        assertNotEquals("945.0",Double.toString(output1));
	}
	
	//+ve Test
	@Test
	public void testforDiscountWithOutGroceryPositive() {		
        Users empUser =  new EmployeeUser();
        double billAmt=990.0;
        boolean grocery = false;
        
        System.out.println("\nTest for applicable discount without grocery");
        double output1 = UtilityClass.calculateDiscount(empUser, billAmt, grocery);
        assertEquals("693.0",Double.toString(output1));
        
	}
	
	@Test 
	public void testforDiscountOnLessAmt() {		
        Users empUser =  new EmployeeUser();
        double billAmt=99.0;
        boolean grocery = false;
        
        System.out.println("\nTest for applicable discount for amount less than 100");
        double output2 = UtilityClass.calculateDiscount(empUser, billAmt, grocery);
        assertNotEquals("99.0",Double.toString(output2));
	}

}
