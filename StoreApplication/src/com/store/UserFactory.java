package com.store;

import com.store.AffiliateUser;
import com.store.Constants;
import com.store.EmployeeUser;
import com.store.Users;

public class UserFactory {
	
	public Users getInstance(String input) {
		if(input.equals(Constants.AFFILIATEUSER)){
			return new AffiliateUser();
		}else if(input.equals(Constants.EMPLOYEEUSER)){
			return new EmployeeUser();
		}else if(input.equals(Constants.CUSTOMERUSER)){
			return new EmployeeUser();
		}
		return null;
	}

}
