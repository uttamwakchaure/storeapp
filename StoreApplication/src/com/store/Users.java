package com.store;

/**
 * Created by uwakchau on 12/20/2018.
 */
public interface Users {
    public String getUserType();
    public double getUserDiscount();
    public Boolean IsGroceryItem(Boolean val);
    public double billAmount(double billamt);
    
}
